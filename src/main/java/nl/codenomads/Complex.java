package nl.codenomads;

/**
 * A class to represent a Complex number with real and imaginary parts.
 */
public class Complex {
    final private Double x, y;

    /**
     * Construct a {@link Complex} object with the given x and y {@link Double} values
     * @param x the x {@link Double} value to use
     * @param y the y {@link Double} value to use
     */
    public Complex(final Double x, final Double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return the x {@link Double} value
     */
    public Double getX() {
        return x;
    }

    /**
     * @return the y {@link Double} value
     */
    public Double getY() {
        return y;
    }
}

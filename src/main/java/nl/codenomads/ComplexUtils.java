package nl.codenomads;

import java.util.function.DoubleBinaryOperator;

/**
 * Utility method for the {@link Complex} class
 */
public class ComplexUtils {
    private static DoubleBinaryOperator addFunction = (var x, var y) -> x + y;

    /**
     * Add the real and imaginary parts of the given {@link Complex}
     * @param complex the {@link Complex} to use
     * @return the {@link Double} of the real and imaginary parts added.
     */
    public static Double addParts(final Complex complex) {
        return addFunction.applyAsDouble(complex.getX(), complex.getY());
    }

    /**
     * Format the given {@link Complex} as a {@link String}
     * @param complex the {@link Complex} to use
     * @return the {@link String} formatted with the real and imaginary parts.
     */
    public static String toString(final Complex complex) {
       return String.format("Complex: real: %.2f imaginary: %.2f", complex.getX(), complex.getY());
    }
}

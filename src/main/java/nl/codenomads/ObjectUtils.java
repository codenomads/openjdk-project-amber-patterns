package nl.codenomads;

/**
 * Utility class to show example of OpenJDK Project Amber language features, JDK 11 or 12.
 */
public class ObjectUtils {
    /**
     * For the given {@link Object} return a {@link String} formatted depending on it's class.
     * @param object the {@link Object} to use
     * @return the formatted {@link String} prefixed with Character, Integer or String and the value of {@link Object}
     */
    public static String toString(final Object object) {
        switch (object) {
            case null: throw new AssertionError("Null object given");
            case Character character : return String.format("character: %c", character);
            case Integer integer : return String.format("Integer: %d", integer);
            case String string : return String.format("String: %s", string);
            default: throw new AssertionError("Object not handled");
        }
    }
}

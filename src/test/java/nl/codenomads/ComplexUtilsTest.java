package nl.codenomads;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for {@link Complex} class.
 */
public class ComplexUtilsTest {
    @Test
    public void testToString() {
        var complex = new Complex(123d, 423d);
        assertEquals("Expected Complex: real: 123 imaginary: 423",
                "Complex: real: 123.00 imaginary: 423.00", ComplexUtils.toString(complex));
    }

    @Test
    public void testBiFunction() {
        var complex = new Complex(213d, 4233d);
        assertEquals("Expected Complex: real: 213 imaginary: 423",
                Double.valueOf(4446d), ComplexUtils.addParts(complex));
    }
}

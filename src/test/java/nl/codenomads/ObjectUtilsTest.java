package nl.codenomads;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for {@link ObjectUtils}
 */
public class ObjectUtilsTest {
    @Test
    public void testToString() {
        assertEquals("Expected character: c", "character: c", ObjectUtils.toString('c'));
        assertEquals("Expected Integer: 1", "Integer: 1", ObjectUtils.toString(1));
        assertEquals("Expected String: a String", "String: a String", ObjectUtils.toString("a String"));
    }

    @Test(expected = AssertionError.class)
    public void testToStringException() {
       ObjectUtils.toString(123L);
    }

    @Test(expected = AssertionError.class)
    public void testToStringNullException() {
        ObjectUtils.toString(null);
    }
}
